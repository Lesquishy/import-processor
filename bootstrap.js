/*
 * ImportProcessor bootstrap file
 * =============================
 *
 * Handles the fundamental loading of the application by simply instantiating the required
 * classes for the program to operate.
 *
 */

const { Observable, of, bindCallback, bindNodeCallback, from, throwError } = require('rxjs');
const { toArray, tap, map, concatMap, switchMap, filter, first, catchError } = require('rxjs/operators');

const Application = require("./classes/Application");
const TitleHandler = require("./classes/TitleHandler");
const importProcessor = new Application();
const titleHandler = new TitleHandler( importProcessor );

const setup = () => {
    // Load the config from 'settings.cfg'. If the process encounters an error, it will be reported here.
    return importProcessor.loadConfig("settings.cfg").pipe(
        // Throw an error if one is raised. This helps identify the cause of a propogated error.
        catchError( err => {
            throw "Unable to load configuration file. Err: " + err
        }),
        // Now that we've loaded our config, switch the stream over to loadCache using concatMap.
        concatMap( () => from( importProcessor.loadCache() ) ),
        tap(cache => console.log( 'cache', cache, 'loaded sucessfully' ) )
    ).toPromise();
}

const main = () => {
    importProcessor.fetchItems().pipe(
        catchError( err => throwError('Unable to fetch items: ' + err) ),
        switchMap( items => titleHandler.filterItems( items ) ),
        catchError( err => throwError('Encountered error when filtering items:\n' + err) )
    ).subscribe(
        next => console.log('next', next),
        err => console.log('\n** Critical Error **\n\n' + err),
        () => console.log("All done")
    );
}

setup().then(
    main,
    err => console.log('[CRITICAL] Failure to setup application with err:', err)
);
