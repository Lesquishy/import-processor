const Application = require("./Application");

const { zip, concat, of, pipe, from } = require("rxjs");
const { delay, mergeMap, toArray, flatMap, pluck, switchMap, map, last, take, scan,
        concatMap, tap, filter, mergeAll, combineLatest, distinctUntilChanged,
        mergeScan } = require("rxjs/operators");


module.exports = class TitleHandler {
    constructor( owner ){
        if( typeof owner instanceof Application ) throw "Invalid owner class passed to TitleHandler constructor. Must be of type Application."

        this.owner = owner;
    }

    assembleFilters(){
        const config = this.owner.config;

        // Create a stream of the remove filters; mapping each to a replace filter of ''
        const removeFilters = of( config ).pipe(
            pluck( "filters", "remove" ),
            map( x => [ "\\b(?:" + x.join("|") + ")\\b", "", "REMOVE" ] )
        );

        // Create a stream of the replace filters, filtering out any filters that are disabled inside
        // the applications configuration file.
        const replaceFilters = of( config ).pipe(
            pluck( "filters", "replace" ),
            flatMap( p => p ),
            map( p => {
                p[2] = p[2] || 'UNNAMED';
                return p
            })
        )

        // Return a stream of the filters, concatting the remove filters and the replace filters together.
        return concat( removeFilters, replaceFilters ).pipe(
            toArray(),
            flatMap( x => x ),
            filter( x => config.settings.filter[x[2]] )
        );
    };


    filterItems( items ){
        const filterStream = this.assembleFilters();
        const itemStream = from( items );

        const _apply = title => filterStream.pipe(
            map( filter => title.replace( new RegExp( filter[0], "gi" ), filter[1] ) ),
            tap( out => title = out )
        )

        const _applyWithCallback = title => {
            // TODO
        }

        const applyFilter = item =>
            of( item ).pipe( concatMap( title => _apply( title ) ), last() );

        return itemStream.pipe(
            concatMap( item => applyFilter( item ) ),
            toArray()
        );
    }
};
