/*
 * ImportProcessor Application Class (core)
 * ========================================
 *
 * Handles core functionality of the processor, such as exploring directories, forming
 * queues for processing and handling async operations.
 *
 */

const fs = require('fs');
const path = require('path');
const { Observable, from, of, bindCallback, bindNodeCallback } = require('rxjs');
const { toArray, tap, map, flatMap, mergeMap, takeWhile, finalize,
        switchMap, pluck, concatMap, filter, first, catchError } = require('rxjs/operators');

const fsReadDirObservable = bindNodeCallback(fs.readdir);
const fsStatObservable = bindNodeCallback(fs.stat);
const fsExistsObservable = bindCallback(fs.exists);
const fsReadObservable = bindNodeCallback(fs.readFile);
const fsWriteObservable = bindNodeCallback(fs.writeFile);

module.exports = class Application {
    constructor(){
        this.config = {};
        this.cache = {};
    };

    /*
     * Loads the application configuration from the path provided.
     *
     * If the path does not exist, or the JSON is malformed, an error will
     * be thrown and the config will not be loaded.
     *
     */
    loadConfig( path ){
        return fsExistsObservable( path ).pipe( // Check if path exists
            tap(doesExist => {
                // If not, throw an error
                if(!doesExist) throw 'Configuration file "'+path+'" not found.'
            }),
            switchMap( () => fsReadObservable( path, 'utf8' ) ), // Otherwise, read contents
            switchMap( content => of( JSON.parse( content ) ) ), // Parse to JSON
            tap(config => {
                if( !config ) throw 'Malformed configuration file "'+path+'".' // If JSON parse failed, throw error
                this.config = config; // Set config on instance.
            }),
            first() // Only take one emission before completing the stream
        );
    };

    /*
     * Loads the application cache; used to store details of stored/processed movies
     * between execution runs.
     *
     * If the cache property is missing (incomplete application config), the JSON is malformed, or the path
     * doesn't exist, the function will load a default empty cache. This will stop the application from,
     * however data stored in malformed JSON will be lost when the cache is resaved.
     *
     * The cache file is stored at the location specified by 'settings.cacheFile' in the
     * applications config JSON.
     *
     */
    loadCache(){
        return of( this.config ).pipe(
            pluck("settings", "cacheFile"), // Fetch the settings.cacheFile property from the config
            tap(path => {
                // Check to ensure we got a value; if not, we'll need to fallback to a default later.
                if( !path ) throw 'Cache file property missing';
            }),
            tap(async (path) => {
                // Use tap and async/await to run the async IO operation (exists).
                // If false is returned, throw an error. Otherwise continue with pipes.
                // Convert the below Observable to a Promise so we can halt execution
                // until the read operation is complete.
                await fsExistsObservable( path ).pipe(
                    tap(doesExist => {
                        if( !doesExist ) throw 'Nominated cache file "'+path+'" does not exist';
                    }),
                    first()
                ).toPromise();
            }),
            // Read content from file
            switchMap( path => fsReadObservable( path, 'utf8' ) ),
            // Parse JSON
            switchMap( content => of( JSON.parse( content ) ) ),
            // If the cache is undefined/false, throw an error. Otherwise set the cache on the instance
            tap(cache => {
                if( !cache ) throw 'Malformed cache file';
                this.cache = cache;
            }),
            // If we encountered an error, default to empty cache and display a warning.
            catchError(err => {
                console.log("[WARN] Unable to load cache file: " + err + ". Falling back to default cache");
                this.cache = { epShows: [] };

                // Return an observable *of* the default cache to keep RxJS happy.
                return of( this.cache );
            }),
            first() // Only take the first result of this observable (ie: auto cancel).
        );
    };

    /*
     * Saves the content of the applications cache to file. The file is located at the path specified
     * in the settings.cacheFile property inside the applications config file.
     *
     * If the cache property doesn't exist, the operation will abort without raising further error. However
     * the function will resolve it's stream with 'false'. This value indicates the failure of the function.
     *
     * If the function succeeds in saving the cache, the return value will be the instances cache.
     *
     */
    saveCache(){
        return of( this.config ).pipe(
            pluck("settings", "cacheFile"),
            tap(path => {
                if( !path ) throw 'Cache file property missing';
            }),
            switchMap(path => {
                return of( this.cache ).pipe(
                    tap( obj => JSON.stringify( obj ) ),
                    concatMap( str => fsWriteObservable( path, str, 'utf8' ) ),
                    map( x => this.cache )
                )
            }),
            catchError(err => {
                console.log("[WARN] Unable to save cache file: " + err + ".");

                // Return an observable *of* FALSE. Indicates failure, still compliant with RxJS.
                return of( false );
            }),
            first() // Only take the first result of this observable (ie: auto cancel).
        );
    };

    /*
     * Uses the Application classes static 'explore' method to explore the directory tree which is located at
     * the path specified as settings.inputDir in the applications config file.
     *
     */
    fetchItems() {
        return Application.explore( this.config.settings.inputDir, [
            /\.sw(?:p|o)/,
            ".git",
            "node_modules",
            /^\./
        ]).pipe( toArray() );
    };

    /*
     * Explores the target directory recursively using RxJS to take advantage of Node's unblocking
     * IO.
     *
     * The function accepts a string as the path/dir to search, and a blacklist; the blacklist is an array
     * of strings or Regex matches that will be tested against the target path *name*, not entire path. If
     * any item in the blacklist is matched against the pathname, the item is skipped.
     *
     * The output of this function can be observed by RxJS and used in conjunction with the 'toArray'
     * operator (eg: explore().pipe( toArray() ).subscribe( x => console.log( x ) );)
     *
     */
    static explore(target, blacklist) {
        return fsReadDirObservable(target).pipe(
            flatMap(p => p),
            filter(p => {
                for( var i = 0; i < blacklist.length; i++ )
                    if( p.match( blacklist[i] ) )
                        return false;

                return true;
            }),
            map( p => path.join( target, p ) ),
            flatMap(p => fsStatObservable(p).pipe(
                map( stat => [p, stat.isDirectory()] )
            )),
            flatMap(details => {
                if( details[1] ) return Application.explore( details[0], blacklist )

                return of( details[0] );
            }),
            catchError( err => {
                throw "Unable to explore target '" + target + "' due to error: " + err
            }),
        );
    };
};

